# How to setup a Q Root Node

## Setup your Server

The Q Root Node is required to run on a server or (virtual) machine on linux. One possibility is to use a local machine, alternatively you can use a cloud instance on AWS for example. A good external tutorial on how to get started with Ethereum on AWS can be found [here](https://medium.com/@pilankar.akshay3/how-to-setup-a-ethereum-poa-private-proof-of-authority-ethereum-network-network-on-amazon-aws-5fdf56d2ad93). Any other linux machine will work as well if it meets the following requirements:

  - Linux machine with SSH access;
  - Min. 1(v)Core (x86), 20 GB storage and 2 GB RAM;
  - Rec. 2(v)Cores (x86), 30 GB storage and 4 GB RAM;
  - Installed applications: docker, docker-compose, git (optional).

### Application Installation

If you are running Ubuntu, use these commands to install all mentioned required applications using `apt`:

Update apt:

```text
$ sudo apt-get update
```

Install git:

```text
$ sudo apt-get install git
```

Install docker:

```text
$ sudo apt-get install docker
```

Install docker-compose:

```text
$ sudo apt-get install docker-compose
```

Please check corresponding online resources for your operating system and the third party application you want to install for further questions.

## Basic Configuration

Clone the repository

```text
$ git clone https://gitlab.com/q-dev/testnet-public-tools
```

and go to the `/testnet-rootnode` directory

```text
$ cd testnet-public-tools/testnet-rootnode
```

This directory contains the `docker-compose.yaml` file for quick launching of the root node with preconfigurations using `.env` file (which can be created from `.env.example` file).

> **Note: ** *If git is not installed on your machine, you can manually copy all files from public repo `testnet-public-tools` onto your machine. Using git is much more comfortable, since it allows pulling file updates with one single command.*

## Set Password for Keystore File

To act as a root node, your node needs a keypair to sign transactions and L0 governance messages. First, create a `/keystore` directory with

```text
$ mkdir keystore
```

then create a file `pwd.txt`

```text
$ nano keystore/pwd.txt
```

then set a password that will be used for future account unlocking by entering it into `pwd.txt`. The password needs to be entered at the beginning of the file. Save your changes with `CTRL+O`, then close nano with `CTRL+X` (if you use a different editor, commands might be different).

## Generate a Keypair

Assuming you are in `/testnet-rootnode` directory, issue this command in order to generate a keypair:  

```text
$ docker-compose run --rm --entrypoint "geth account new --datadir=/data --password=/data/keystore/pwd.txt" testnet-rootnode
```

The output of this command should look like this:

```text
Your new key was generated

Public address of the key:   0xb3FF24F818b0ff6Cc50de951bcB8f86b52287dac
Path of the secret key file: /data/keystore/UTC--2021-01-18T11-36-28.705754426Z--b3ff24f818b0ff6cc50de951bcb8f86b52287dac

- You can share your public address with anyone. Others need it to interact with you.
- You must NEVER share the secret key with anyone! The key controls access to your funds!
- You must BACKUP your key file! Without the key, it's impossible to access account funds!
- You must REMEMBER your password! Without the password, it's impossible to decrypt the key!
```

This way, a new private key is generated and stored in docker container in `data/keystore` directory encrypted with password from `pwd.txt` file. In our example, *0xb3FF24F818b0ff6Cc50de951bcB8f86b52287DAc* (**you will have a different value**) is the address corresponding to the newly generated private key.

*Alternatively*, you can generate a secret key pair and corresponding file [here](https://vanity-eth.tk/) and save it to the `/keystore` directory manually.
Also you may use `create-geth-private-key.js` script in `/js-tools` folder.

Whether you chose to provide your own vanity keys or use the above command to create a keypair, please ensure that the directory `/keystore` contains the following files:

```text
testnet-rootnode
|   ...
|   ...
└ keystore
  |   UTC--2021-01-18T11-36-28.705754426Z--b3ff24f818b0ff6cc50de951bcb8f86b52287dac
  |   pwd.txt
```

> **Note: ** *Following our example, pwd.txt contains the password to encrypted file "UTC--2021-01-18T11-36-28.705754426Z--b3ff24f818b0ff6cc50de951bcb8f86b52287dac" in clear text.*

If you want to change the password in the future, you need to stop the node first.

```text
$ docker-compose down
```

Then start password reset procedure with

```text
$ docker-compose run testnet-rootnode --datadir /data account update 0xb3ff24f818b0ff6cc50de951bcb8f86b52287dac
```

> **Note: ** *You need to remove address _0xb3ff24f818b0ff6cc50de951bcb8f86b52287dac_ and add your account address instead.*

## Configure Node

Copy `.env.example` to `.env` and edit this file in `/testnet-rootnode` directory:

```text
$ cp .env.example .env
$ nano .env
```

Enter your (newly created) root node address without leading 0x here:

```text
# your q address here (without leading 0x)
ADDRESS=b3FF24F818b0ff6Cc50de951bcB8f86b52287DAc
```

Then add your machines public IP address (please make sure your machine is reachable at the corresponding IP since it's required for discoverability by other network participants) here:

```text
# your public IP address here
IP=193.19.228.94
```

Optionally choose a port for p2p protocol or just leave default value (use different ports for every node you are running):

```text
# the port you want to use for p2p communication (default is 30313)
EXT_PORT=30313
```

The resulting `.env` file should look somehow like this:

```text
# docker image for q client
QCLIENT_IMAGE=qblockchain/q-client:1.2.2

# your q address here (without leading 0x)
ADDRESS=b3FF24F818b0ff6Cc50de951bcB8f86b52287DAc

# your public IP address here
IP=193.19.228.94

# the port you want to use for p2p communication (default is 30313)
EXT_PORT=30313

# extra bootnode you want to use
BOOTNODE1_ADDR=enode://c610793186e4f719c1ace0983459c6ec7984d676e4a323681a1cbc8a67f506d1eccc4e164e53c2929019ed0e5cfc1bc800662d6fb47c36e978ab94c417031ac8@79.125.97.227:30304
BOOTNODE2_ADDR=enode://8eff01a7e5a66c5630cbd22149e069bbf8a8a22370cef61b232179e21ba8c7b74d40e8ee5aa62c54d145f7fc671b851e5ccbfe124fce75944cf1b06e29c55c80@79.125.97.227:30305
BOOTNODE3_ADDR=enode://7a8ade64b79961a7752daedc4104ca4b79f1a67a10ea5c9721e7115d820dbe7599fe9e03c9c315081ccf6a2afb0b6652ee4965e38f066fe5bf129abd6d26df58@79.125.97.227:30306
```

## Add your Root Node to https://stats.qtestnet.org

If you want your root node to report to the [network statistics](https://stats.qtestnet.org), you can add an additional flag to the node entrypoint within file `/testnet-rootnode/docker-compose.yaml`, it should look like this:

```text
testnet-rootnode:
  image: $QCLIENT_IMAGE
  entrypoint: ["geth", "--ethstats=<Your_RootNode_Name>:<Testnet_access_key>@stats.qtestnet.org", "--datadir=/data", ...]
```

`<Your_RootNode_Name>` can be chosen arbitrarily. It will be displayed in the statistics and could be something like "OurCoolCompany - 0xABC123". You can use special characters, emojis as well as spaces. We would appreciate to include the beginning of your Root Node Q address, so there is a link between your client and your address.

In order to find out the `<Testnet_access_key>` please join [Q Discord Server](https://discord.gg/YTgkvJvZGD) and find stats key in [**🔑│testnet-key** channel](https://discord.com/channels/902893347239247952/1042401601639432212).

## Launch Root Node

Now launch your root node using docker-compose file in rootnode directory:

```text
$ docker-compose up -d
```

Check your nodes real-time logs with the following command:

```text
$ docker-compose logs -f --tail "100"
```

## Find additional peers

In case your client can't connect with the default configuration, we recommend that you add an additional flag referring to one of our additional peers (`$BOOTNODE1_ADDR`, `$BOOTNODE2_ADDR`or `$BOOTNODE3_ADDR`) within `docker-compose.yaml` file:

```text
testnet-rootnode:
  image: $QCLIENT_IMAGE
  entrypoint: ["geth", "--bootnodes=$BOOTNODE1_ADDR,$BOOTNODE2_ADDR,$BOOTNODE3_ADDR", "--datadir=/data", ...]
```

## Get Q Tokens

In order to become a root node, you will need to make an onchain proposal to [add yourself to the root node panel](how-to-become-a-root-node.md). You need Q tokens for this. For Q testnet, you can get some Q using the [faucet](https://faucet.qtestnet.org/). Check the [faucet documentation](https://docs.qtestnet.org/how-to-install-metamask/#faucet) for more information. Finally, please verify that tokens were sent by looking up your address within [Block Explorer](https://explorer.qtestnet.org/).

## Put Stake in Rootnodes Contract

As was mentioned previously, you should put stake to rootnodes contract in order to become a rootnode.

You can use the dApp "Your HQ" that can be found at [https://hq.qtestnet.org](https://hq.qtestnet.org). Go to `Consensus Services` -> `Root Node Staking` for stake management. Also, you may want to check our [Consensus Services documentation](dapp-consensusservices.md).

## Updating Q-Client & Docker Images

To upgrade the node follow the instructions [Upgrade Node](how-to-upgrade-node.md)
